package purchase

import "fmt"

// NeedsLicense determines whether a license is needed to drive a type of vehicle. Only "car" and "truck" require a license.
func NeedsLicense(kind string) bool {
    if ((kind == "car") || (kind == "truck")) {
        return true
    }
    return false
}

// ChooseVehicle recommends a vehicle for selection. It always recommends the vehicle that comes first in dictionary order.
func ChooseVehicle(option1, option2 string) string {
    if (option1 < option2) {
        answer := fmt.Sprintf("%s is clearly the better choice.", option1)
        return  answer
    }
    answer := fmt.Sprintf("%s is clearly the better choice.", option2)
    return  answer
}

// CalculateResellPrice calculates how much a vehicle can resell for at a certain age.
func CalculateResellPrice(originalPrice, age float64) float64 {
    newPrice := originalPrice
    if (age < 3) {
        newPrice = originalPrice * 0.80
    } else if (age < 10) {
        newPrice = originalPrice * 0.70
    } else if (age >= 10) {
        newPrice = originalPrice * 0.50
    }
    return newPrice

}
