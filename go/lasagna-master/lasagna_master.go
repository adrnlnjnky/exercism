package lasagna

func PreparationTime(layers []string, prepTime int) int {
    if (prepTime == 0) { prepTime = 2}
    return len(layers) * prepTime
}

func Quantities(layers []string) (int, float64) {
    var noodles int
    var sauce float64

    for i := 0; i < len(layers); i++ {
        layer := layers[i]
        if (layer == "noodles") {noodles += 50}
        if (layer == "sauce")   {sauce += 0.2}
    }
    return noodles, sauce
}

// TODO: define the 'AddSecretIngredient()' function
func AddSecretIngredient(friendsList, myList []string) {

    hisLast := len(friendsList)
    ingredient := friendsList[hisLast - 1]
    myLast := len(myList)
    myList[myLast - 1] = ingredient
}

// TODO: define the 'ScaleRecipe()' function
func ScaleRecipe(quantities []float64, portions int) []float64 {
    scaledQuantities := make([]float64, 0, len(quantities))
    for _, quantity := range quantities {
      scaledQuantities = append(scaledQuantities, (quantity/2)*float64(portions))
    }
    return scaledQuantities
}
