package techpalace

import "strings"

// WelcomeMessage returns a welcome message for the customer.
func WelcomeMessage(customer string) string {
    greeting := "Welcome to the Tech Palace, " + strings.ToUpper(customer)
    return greeting
}


// AddBorder adds a border to a welcome message.
func AddBorder(welcomeMsg string, numStarsPerLine int) string {
    var stars string
    for i := 0; i < numStarsPerLine; i++ {
        stars = stars + "*"
    }
    greeting := stars + "\n" + welcomeMsg + "\n" + stars
    return greeting

}

// CleanupMessage cleans up an old marketing message.
func CleanupMessage(oldMsg string) string {
    cleaned := strings.Trim(oldMsg, "*\n")
    cleaned = strings.TrimSpace(cleaned)
    return cleaned

}
