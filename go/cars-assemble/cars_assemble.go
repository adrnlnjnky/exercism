package cars

// CalculateWorkingCarsPerHour calculates how many working cars are
// produced by the assembly line every hour
func CalculateWorkingCarsPerHour(productionRate int, successRate float64) float64 {
    speed := float64(productionRate)
    produced := (speed * successRate)/100
    return produced

}

// CalculateWorkingCarsPerMinute calculates how many working cars are
// produced by the assembly line every minute
func CalculateWorkingCarsPerMinute(productionRate int, successRate float64) int {
    hourly := CalculateWorkingCarsPerHour(productionRate, successRate)
    minutly := int(hourly / 60)
    return minutly
}

// CalculateCost works out the cost of producing the given number of cars
func CalculateCost(carsCount int) uint {
    groups := carsCount / 10
    singles := carsCount % 10

    cost := uint(groups * 95000 + singles * 10000)

    return cost
}
