package lasagna

const OvenTime = 40

func RemainingOvenTime(actualMinutesInOven int) int {
    remaining := OvenTime - actualMinutesInOven
    return remaining
}

// PreparationTime calculates the time needed to prepare the lasagna based on the amount of layers.
func PreparationTime(numberOfLayers int) int {
    preptime := numberOfLayers* 2
    return preptime
}

// ElapsedTime calculates the total time needed to create and bake a lasagna.
func ElapsedTime(numberOfLayers, actualMinutesInOven int) int {
    preptime := PreparationTime(numberOfLayers)
    worktime := preptime + actualMinutesInOven
    return worktime
}
