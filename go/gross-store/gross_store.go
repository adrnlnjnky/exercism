package gross

// Units stores the Gross Store unit measurements.
func Units() map[string]int {
    units := make(map[string]int)
    units["quarter_of_a_dozen"] = 3
    units["half_of_a_dozen"] = 6
    units["dozen"] = 12
    units["small_gross"] = 120
    units["gross"] = 144
    units["great_gross"] = 1728

    return units

}

// NewBill creates a new bill.
func NewBill() map[string]int {
    bill := make(map[string]int)
    return bill
}

// AddItem adds an item to customer bill.
func AddItem(bill, units map[string]int, item, unit string) bool {
    new_units, exists := units[unit]
    if exists == false { return false }
    existing, exists := bill[item]
    if exists == true {
        bill[item] = (existing + new_units)
    } else {
        bill[item] = new_units
    }
    return true
}


// RemoveItem removes an item from customer bill.
func RemoveItem(bill, units map[string]int, item, unit string) bool {
    toRemove, itemExists := units[unit]
    itemsOnBill, unitExists := bill[item]

    newItems := itemsOnBill - toRemove

    switch {
    case !itemExists: return false
    case !unitExists: return false
    case newItems < 0: return false
    case newItems == 0: delete(bill, item)
    default: bill[item] = newItems
    }
    return true
}

// GetItem returns the quantity of an item that the customer has in his/her bill.
func GetItem(bill map[string]int, item string) (int, bool) {
    units, exists := bill[item]
    if exists == false { return 0, false}
    return units, true
}



















