package partyrobot

import "fmt"

// Welcome greets a person by name.
func Welcome(name string) string {
    welcome := fmt.Sprintf("Welcome to my party, %s!", name)
    return welcome
}

// HappyBirthday wishes happy birthday to the birthday person and exclaims their age.
func HappyBirthday(name string, age int) string {
    return "Happy birthday " + name + "! You are now " + fmt.Sprint(age) + " years old!"
}

// AssignTable assigns a table to each guest.
func AssignTable(name string, table int, neighbor, direction string, distance float64) string {
    greet := fmt.Sprint(Welcome(name), "\n")
    assignedTable := fmt.Sprintf("You have been assigned to table %03d. ", table)
    directions := fmt.Sprintf("Your table is %s, exactly %.1f meters from here.\n", direction, distance)
    seat := fmt.Sprintf("You will be sitting next to %s.", neighbor)

    return greet + assignedTable + directions + seat
}
