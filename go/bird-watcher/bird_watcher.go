package birdwatcher

// TotalBirdCount return the total bird count by summing
// the individual day's counts.
func TotalBirdCount(birdsPerDay []int) int {
    var totalBirds int
    for i := 0; i < len(birdsPerDay); i++ {
        x := birdsPerDay[i]
        totalBirds += x
    }
    return totalBirds
}

// BirdsInWeek returns the total bird count by summing
// only the items belonging to the given week.
func BirdsInWeek(birdsPerDay []int, week int) int {
    var totalBirds int
    index := (week - 1) * 7
    for i := index; i < index + 7; i++ {
        x := birdsPerDay[i]
        totalBirds += x
    }
    return totalBirds
}

// FixBirdCountLog returns the bird counts after correcting
// the bird counts for alternate days.
func FixBirdCountLog(birdsPerDay []int) []int {
    for i := 0; i < len(birdsPerDay); i += 2 {
        birdsPerDay[i]++
    }
    return birdsPerDay
}
