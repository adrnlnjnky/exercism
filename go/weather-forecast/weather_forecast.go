// Package weather provides tools for working with weather data.
package weather

// CurrentCondition describes the weather currently.
var CurrentCondition string
// CurrentLocation describes the current location.
var CurrentLocation string

// Forecast returns the forecat given the city and condition.
func Forecast(city, condition string) string {
	CurrentLocation, CurrentCondition = city, condition
	return CurrentLocation + " - current weather condition: " + CurrentCondition
}
