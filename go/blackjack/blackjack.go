package blackjack

// ParseCard returns the integer value of a card following blackjack ruleset.
func ParseCard(card string) int {
    value := 0
    switch card {
    case "ace": value = 11
    case "king": value = 10
    case "queen": value = 10
    case "jack": value = 10
    case "ten": value = 10
    case "nine": value = 9
    case "eight": value = 8
    case "seven": value = 7
    case "six": value = 6
    case "five": value = 5
    case "four": value = 4
    case "three": value = 3
    case "two": value = 2
    case "one": value = 1
   }
   return value
}

// IsBlackjack returns true if the player has a blackjack, false otherwise.
func IsBlackjack(card1, card2 string) bool {
    value1 := ParseCard(card1)
    value2 := ParseCard(card2)

    blackjack := false
    switch {
       case value1 + value2 == 21: blackjack = true
    }
    return blackjack
}

// LargeHand implements the decision tree for hand scores larger than 20 points.
func LargeHand(isBlackjack bool, dealerScore int) string {

    switch {
    case (isBlackjack && (dealerScore < 10)): return "W"
    case (isBlackjack && (dealerScore > 9)): return "S"
    }
    return "P"
}

// SmallHand implements the decision tree for hand scores with less than 21 points.
func SmallHand(handScore, dealerScore int) string {
    switch {
    case handScore >= 17: return "S"
    case handScore <= 11: return "H"
    case dealerScore >= 7: return "H"
    case dealerScore < 7: return "S"
    case handScore == 22: return "P"
    }
    return "P"
}



