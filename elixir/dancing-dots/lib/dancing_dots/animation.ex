defmodule DancingDots.Animation do
  @callback init(opts :: any) :: any
  @type dot :: DancingDots.Dot.t()
  @type opts :: keyword
  @type error :: any
  @type frame :: pos_integer

  @callback init(opts) :: {:ok, opts} | {:error, error}
  @callback handle_frame(dot, frame, opts) :: dot

  defmacro __using__(_) do
    quote do
      @behaviour DancingDots.Animation
      def init(opts), do: {:ok, opts}
      defoverridable init: 1
    end
  end
end

defmodule DancingDots.Flicker do
  use DancingDots.Animation

  @blink 4
  @opacity_change 0.5

  @impl DancingDots.Animation
  def handle_frame(%DancingDots.Dot{opacity: opacity} = dot, frame, _opts) do
    if rem(frame, @blink) == 0 do
      %{dot | opacity: opacity * @opacity_change}
    else
      dot
    end
  end
end

defmodule DancingDots.Zoom do
  use DancingDots.Animation

  @impl DancingDots.Animation
  def init(opts) do
    velocity = opts[:velocity]

    if !is_number(velocity) do
      {:error,
       "The :velocity option is required, and its value must be a number. Got: #{inspect(velocity)}"}
    else
      {:ok, opts}
    end
  end

  @impl DancingDots.Animation
  def handle_frame(%DancingDots.Dot{radius: radius} = dot, frame, velocity: velocity) do
    %{dot | radius: (frame - 1) * velocity + radius}
  end
end
