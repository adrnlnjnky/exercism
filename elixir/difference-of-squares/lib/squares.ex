defmodule Squares do
  @moduledoc """
  Calculate sum of squares, square of sum, difference between two sums from 1 to a given end number.
  """

  @doc """
  Calculate sum of squares from 1 to a given end number.
  """
  @spec sum_of_squares(pos_integer) :: pos_integer
  def sum_of_squares(number), do: sum_of_squares(number, 0)

  defp sum_of_squares(0, result), do: result
  defp sum_of_squares(n, r), do: sum_of_squares(n - 1, :math.pow(n, 2) + r)

  @doc """
  Calculate square of sum from 1 to a given end number.
  """
  @spec square_of_sum(pos_integer) :: pos_integer
  def square_of_sum(number), do: square_of_sum(number, 0)

  defp square_of_sum(0, r), do: :math.pow(r, 2)
  defp square_of_sum(n, r), do: square_of_sum(n- 1, r + n)

  @doc """
  Calculate difference between sum of squares and square of sum from 1 to a given end number.
  """
  @spec difference(pos_integer) :: pos_integer
  def difference(number) do
    square_of_sum(number) - sum_of_squares(number)
  end
end
