defmodule CryptoSquare do
  @punctuation ["@", "%", ",", ".", "?", ",", ":", ";", "!", "(", ")", " "]

  @doc """
  Encode string square methods
  ## Examples

    iex> CryptoSquare.encode("abcd")
    "ac bd"
  """
  @spec encode(String.t()) :: String.t()
  def encode(""), do: ""

  def encode(str) do
    str
    |> String.downcase()
    |> String.replace(@punctuation, "")
    |> split_string
    |> sort
    |> List.delete_at(-1)
    |> List.to_string()
  end

  defp split_string(str) do
    size = block_size(str)

    str
    |> String.split("", trim: true)
    |> Enum.chunk_every(size)
    |> Enum.map(&pad_chunks(&1, size))
    |> Enum.map(&Enum.chunk_every(&1, 1))
  end

  defp sort(block, code \\ [])
  defp sort([], code), do: code

  defp sort(block, code) do
    sort(shrink(block), code ++ get_chunk(block))
  end

  defp shrink(block) do
    Enum.map(block, &List.delete_at(&1, 0))
    |> Enum.filter(fn x -> x != [] end)
  end

  defp get_chunk(block) do
    block
    |> Enum.map(&List.first/1)
    |> List.flatten()
    |> List.insert_at(-1, " ")
  end

  defp pad_chunks(block, size) when length(block) < size,
    do: pad_chunks(List.insert_at(block, -1, " "), size)

  defp pad_chunks(block, _size), do: block

  defp block_size(str) do
    str
    |> String.length()
    |> :math.sqrt()
    |> Float.ceil()
    |> Kernel.trunc()
    |> qualify_block
  end

  defp qualify_block(c) when (c - 1) * c >= c - 1 and c >= c - 1 and c - (c - 1) <= 1, do: c
  defp qualify_block(c), do: qualify_block(c - 1)
end
