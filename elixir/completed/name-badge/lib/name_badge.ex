defmodule NameBadge do
  def print(id, name, department) do
    if department != nil do
      dep = String.upcase(department)
      if id == nil do
          "#{name} - #{dep}"
      else
        "[#{id}] - #{name} - #{dep}"
      end
    else
      if id == nil do
        "#{name} - OWNER"
      else
        "[#{id}] - #{name} - OWNER"
      end
    end
  end
end
