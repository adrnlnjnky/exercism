defmodule RunLengthEncoder do
  @doc """
  Generates a string where consecutive elements are represented as a data value and count.
  "AABBBCCCC" => "2A3B4C"
  For this example, assume all input are strings, that are all uppercase letters.
  It should also be able to reconstruct the data into its original form.
  "2A3B4C" => "AABBBCCCC"
  """
  @spec encode(String.t()) :: String.t()
  def encode(string) do
    string
    |> String.split("", trim: true)
    |> Enum.chunk_by(& &1)
    # |> Enum.chunk_by(&Function.identity/1)
    |> Enum.map_join(fn [h | _] = x -> if length(x) == 1, do: "#{h}", else: "#{length(x)}#{h}" end)
  end

  @spec decode(String.t()) :: String.t()
  def decode(string) do
    String.replace(string, ~r/\d+\D/, fn x ->
      {n, l} = Integer.parse(x)
      String.duplicate(l, n)
    end)
  end
end
