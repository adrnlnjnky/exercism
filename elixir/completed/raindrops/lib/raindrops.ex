defmodule Raindrops do
  @map %{3 => "Pling", 5 => "Plang", 7 => "Plong"}

  @doc """
  Returns a string based on raindrop factors.

  - If the number contains 3 as a prime factor, output 'Pling'.
  - If the number contains 5 as a prime factor, output 'Plang'.
  - If the number contains 7 as a prime factor, output 'Plong'.
  - If the number does not contain 3, 5, or 7 as a prime factor,
    just pass the number's digits straight through.
  """
  @spec convert(pos_integer) :: String.t()
  def convert(number) do
    sound = get_sound(number, 3) <> get_sound(number, 5) <> get_sound(number, 7)
    if sound != "", do: sound, else: to_string(number)
  end

  defp get_sound(number, prime) when prime in [3, 5, 7],
    do: if(rem(number, prime) == 0, do: Map.get(@map, prime), else: "")
end
