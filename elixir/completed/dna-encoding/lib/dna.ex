defmodule DNA do
  def encode_nucleotide(code_point) do
    cond do
      code_point == ?A -> 1
      code_point == ?C -> 2
      code_point == ?G -> 4
      code_point == ?T -> 8
      code_point == ?\s -> 0
    end
  end

  def decode_nucleotide(encoded_code) do
    cond do
      encoded_code == 1 -> ?A
      encoded_code == 2 -> ?C
      encoded_code == 4 -> ?G
      encoded_code == 8 -> ?T
      encoded_code == 0 -> ?\s
    end
  end

  def encode(dna), do: do_encode(dna, <<>>)

  defp do_encode([], code), do: code

  defp do_encode([head | tail], code),
    do: do_encode(tail, <<code::bitstring, <<encode_nucleotide(head)::4>>::bitstring>>)

  def decode(dna), do: do_decode(dna, [])

  defp do_decode(<<>>, count), do: count

  defp do_decode(<<head::4, tail::bitstring>>, count) do
    count = count ++ [decode_nucleotide(head)]
    do_decode(tail, count)
  end
end
