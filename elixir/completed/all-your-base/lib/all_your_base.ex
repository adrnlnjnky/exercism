defmodule AllYourBase do
  @doc """
  Given a number in input base, represented as a sequence of digits, converts it to output base,
  or returns an error tuple if either of the bases are less than 2
  """
  @docs """
  converts numbers up and down the base scale

  """
  @spec convert(list, integer, integer) :: {:ok, list} | {:error, String.t()}
  def convert(digits, input_base, output_base) do
    cond do
      output_base < 2 ->
        {:error, "output base must be >= 2"}

      input_base < 2 ->
        {:error, "input base must be >= 2"}

      Enum.any?(digits, &(&1 not in 0..(input_base - 1))) ->
        {:error, "all digits must be >= 0 and < input base"}

      true ->
        {:ok,
         digits
         |> conv_from(input_base, 0)
         |> conv_to(output_base)}
    end
  end

  defp conv_from([], _ib, acc), do: acc

  defp conv_from([d | t], ib, acc),
    do: conv_from(t, ib, acc * ib + d)

  defp conv_to(d, ob)
       when d < ob,
       do: [d]

  defp conv_to(d, ob) do
    conv_to(div(d, ob), ob) ++ [rem(d, ob)]
  end
end
