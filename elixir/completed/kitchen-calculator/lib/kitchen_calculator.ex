defmodule KitchenCalculator do

  def get_volume(volume_pair) do
    elem(volume_pair, 1)
  end

  def to_milliliter(volume_pair = {:cup, _}), do: {:milliliter, elem(volume_pair, 1) * 240}
  def to_milliliter(volume_pair = {:fluid_ounce, _}), do: {:milliliter, elem(volume_pair, 1) * 30}
  def to_milliliter(volume_pair = {:teaspoon, _}), do: {:milliliter, elem(volume_pair, 1) * 5}
  def to_milliliter(volume_pair = {:tablespoon, _}), do: {:milliliter, elem(volume_pair, 1) * 15}
  def to_milliliter(volume_pair = {:milliliter, _}), do: {:milliliter, elem(volume_pair, 1)}
  def to_milliliter(volume_pair = {:milliliters, _}), do: {:milliliter, elem(volume_pair, 1)}

  def from_milliliter(volume_pair, _unit = :cup), do: {:cup, elem(volume_pair, 1) / 240}
  def from_milliliter(volume_pair, _unit = :fluid_ounce), do: {:fluid_ounce, elem(volume_pair, 1) / 30}
  def from_milliliter(volume_pair, _unit = :teaspoon), do: {:teaspoon, elem(volume_pair, 1) / 5}
  def from_milliliter(volume_pair, _unit = :tablespoon), do: {:tablespoon, elem(volume_pair, 1) / 15}
  def from_milliliter(volume_pair, _unit = :milliliter), do: {:milliliter, elem(volume_pair, 1) / 1}
  def from_milliliter(volume_pair, _unit = :milliliters), do: {:milliliter, elem(volume_pair, 1) / 1}

  def convert(volume_pair, unit) do
    to_milliliter(volume_pair) |> from_milliliter(unit)
  end
end
