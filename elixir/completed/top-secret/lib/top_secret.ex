defmodule TopSecret do
  def to_ast(string) do
    {:ok, code} = Code.string_to_quoted(string)
    code
  end

  def decode_secret_message_part(ast = {opt, _meta, [{:when, _, [head | _]}, _]}, acc)
      when opt in [:def, :defp] do
    fname = do_part_decoding(head)
    {ast, [fname | acc]}
  end

  def decode_secret_message_part(ast = {opt, _meta, [head | _data]}, acc)
      when opt in [:def, :defp] do
    fname = do_part_decoding(head)
    {ast, [fname | acc]}
  end

  def decode_secret_message_part(ast, acc), do: {ast, acc}

  defp do_part_decoding({fname, _, tail}) do
    {code, _} =
      fname
      |> to_string()
      |> String.split_at(get_arrity(tail))

    code
  end

  defp get_arrity(func), do: if(func, do: length(func), else: 0)

  def decode_secret_message(string) do
    ast = to_ast(string)
    {_, acc} = Macro.prewalk(ast, [], &decode_secret_message_part/2)
    acc |> Enum.reverse() |> Enum.join()
  end
end
