defmodule ResistorColor do
  @doc """
  Return the value of a color band
  """

  @colorCode ~w(black brown red orange yellow green blue violet grey white)a

  @spec code(atom) :: integer()
  def code(color) do
    Enum.find_index(@colorCode, fn x -> x == color  end)
  end
end
