defmodule PigLatin do


  @vowel ["a", "e", "i", "o", "u"]


  @doc """
  Given a `phrase`, translate it a word at a time to Pig Latin.
  """
  @spec translate(phrase :: String.t()) :: String.t()
  def translate(phrase) do
    phrase
    |> String.split()
    |> Enum.map_join(" ", &make_pig_latin/1)
  end

  defp make_pig_latin(<<f::binary-1, _rest::binary>> = word)
  when f in @vowel do
    "#{word}ay"
  end

  defp make_pig_latin(<<f::binary-1, s::binary-1, rest::binary>>)
  when f in ["y"] and s in @vowel do
    "#{s}#{rest}#{f}ay"
  end

  defp make_pig_latin(<<f::binary-1, s::binary-1, _rest::binary>> = word)
  when f in ["x", "y"] and s not in @vowel do
    "#{word}ay"
  end

  defp make_pig_latin(<<f::binary-3, rest::binary>>)
  when f in ["squ"] do
    "#{rest}#{f}ay"
  end

  defp make_pig_latin(<<f::binary-2, rest::binary>>)
  when f in ["ch", "qu"] do
    "#{rest}#{f}ay"
  end

  defp make_pig_latin(consonants) do
    {head, rest} = break_head(consonants)
    "#{rest}#{head}ay"
  end

  defp break_head(rest, head \\ "")

  defp break_head(<<f::binary-1, rest::binary>>, consts) when f not in @vowel and f not in ["y"] do
    break_head(rest, consts <> f)
  end

  defp break_head(rest, consts) do
    {consts, rest}
  end
end
