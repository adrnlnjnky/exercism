defmodule RemoteControlCar do
  @enforce_keys [:nickname]
  defstruct [:nickname, battery_percentage: 100, distance_driven_in_meters: 0]

  @doc """
  Returns a brand new remote contolled car struct with a nickname provided

    RemoteControlCar.new()
    # => %RemoteControlCar{
      battery_percentage: 100,
      display_distance: 0,
      nickname: "none"
  """
  def new() do
    %RemoteControlCar{nickname: "none"}
  end

  @doc """
  Returns a brand new remote contolled car struct with a nickname provided

    RemoteControlCar.new("Blue")
    # => %RemoteControlCar{
      battery_percentage: 100,
      display_distance: 0,
      nickname: "Blue"
  """
  def new(nickname) do
    %RemoteControlCar{nickname: nickname}
  end

  @doc """
  Returns a distance for display on LED screen

    car = RemoteControlCar.new()
    RemoteControlCar.display_distance(car)

  """
  def display_distance(%RemoteControlCar{} = remote_car) do
    "#{remote_car.distance_driven_in_meters} meters"
    end

  @doc """
  Returns the battery perdcentage for display on the LED display

    car = RemoteControlCar.new()
    RemoteControlCar.display_battery(car)
    
  """
  def display_battery(%RemoteControlCar{} = remote_car) do
      case remote_car.battery_percentage do
        0 -> "Battery empty"
        _ -> "Battery at #{remote_car.battery_percentage}%"
      end
  end

  @doc """
  Updates the number of meters driven by 20
  drains the battery 1%

    %RemoteControlCar{
      battery_percentage: 0,
      distance_driven_in_meters: 1980,
      nickname: "Red"
    }
    |> RemoteControlCar.drive()

    => %RemoteControlCar{
         battery_percentage: 99,
         distance_driven_in_meters: 20,
         nickname: "Red"
       }
  """
  def drive(%RemoteControlCar{} = remote_car) do
    case remote_car.battery_percentage do
      0 -> remote_car
      _ -> %{remote_car | battery_percentage: remote_car.battery_percentage - 1, 
          distance_driven_in_meters: remote_car.distance_driven_in_meters + 20} 
    end
  end

end
