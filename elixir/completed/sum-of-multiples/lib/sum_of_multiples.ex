defmodule SumOfMultiples do
  @doc """
  Adds up all numbers from 1 to a given end number that are multiples of the factors provided.
  """
  @spec to(non_neg_integer, [non_neg_integer]) :: non_neg_integer
  def to(limit, factors) do

    for x <- 1..(limit - 1),
      Enum.any?(factors, &(&1 != 0 && rem(x, &1) == 0 )),
      reduce: 0
    do
        acc -> x + acc
    end



  end
    

    # factors
    # |> Enum.filter(fn x -> x < limit end)
    # |> Enum.map(fn x -> Enum.map(1..limit, fn y -> x * y end) end)
    # |> List.flatten()
    # |> Enum.reject(fn x -> x >= limit end)
    # |> Enum.uniq()
    # |> Enum.sum()
  # end

  # My mentor chriseyre2000 showed me how to switch the logic and get this!!
    # 1..(limit-1)
    # |> Enum.filter(fn x -> Enum.any?(factors, &(&1 > 0 && rem(x, (&1)) == 0 )) end)
    # |> Enum.sum()
  # end

end
