defmodule IsbnVerifier do
  @doc """
    Checks if a string is a valid ISBN-10 identifier

    ## Examples

      iex> IsbnVerifier.isbn?("3-598-21507-X")
      true

      iex> IsbnVerifier.isbn?("3-598-2K507-0")
      false

  """
  @spec isbn?(String.t()) :: boolean
  def isbn?(isbn) do
    String.replace(isbn, "-", "") |> qualify_isbn()
  end

  def qualify_isbn(isbn) do
    cond do
      !String.match?(isbn, ~r/\d{10}|\d{9}X/) ->
        false

      String.length(isbn) != 10 ->
        false

      true ->
        0 ==
          isbn
          |> String.split("", trim: true)
          |> Enum.reverse()
          |> handle_check()
          |> Enum.map(&String.to_integer/1)
          |> Enum.zip_reduce(1..10, 0, fn x, y, acc -> x * y + acc end)
          |> Integer.mod(11)
    end
  end

  defp handle_check([check | body]) do
    check = if check == "X", do: "10", else: check
    List.insert_at(body, 0, check)
  end
end
