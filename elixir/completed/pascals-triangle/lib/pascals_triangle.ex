defmodule PascalsTriangle do
  @doc """
  Calculates the rows of a pascal triangle
  with the given height
  """
  @spec rows(integer) :: [[integer]]
  def rows(num) do
    for n <- 1..num do
      r(n)
    end

  end

  defp r(1), do: [1]
  defp r(n) do
    [0 | r(n - 1)]
    |> Enum.chunk_every(2, 1, [0])
    |> Enum.map(&Enum.sum/1)
  
  end

end
