defmodule Username do


  def sanitize(username = []), do: username
  def sanitize(username) do
    username =
      for char <- username do
        case char do
          first when first == ?_ -> first
          first when first == ?ä -> [?a, ?e]
          first when first == ?ö -> [?o, ?e]
          first when first == ?ü -> [?u, ?e]
          first when first == ?ß -> [?s, ?s]
          first when first < ?a -> ''
          first when first > ?z -> ''
          _ -> char
        end
      end

    # ä becomes ae
    # ö becomes oe
    # ü becomes ue
    # ß becomes ss

    List.flatten(username)
  end
end
