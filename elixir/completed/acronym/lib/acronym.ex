defmodule Acronym do
  @doc """
  Generate an acronym from a string.
  "This is a string" => "TIAS"
  """
  @spec abbreviate(String.t()) :: String.t()
  def abbreviate(string) do


    list = String.split(string, [" ", "-", "_"], trim: true)
    letters =
    for word <- list do
      word
      |> String.first()
      |> String.capitalize()
    end
    |> to_string()
  end
end
