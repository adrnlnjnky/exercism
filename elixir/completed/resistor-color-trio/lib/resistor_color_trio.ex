defmodule ResistorColorTrio do
  @color_map %{
    :black => 0,
    :brown => 1,
    :red => 2,
    :orange => 3,
    :yellow => 4,
    :green => 5,
    :blue => 6,
    :violet => 7,
    :grey => 8,
    :white => 9
  }

  @doc """
  Calculate the resistance value in ohm or kiloohm from resistor colors
  """
  @spec label(colors :: [atom]) :: {number, :ohms | :kiloohms}
  def label(colors) do
    r = value(colors) * Integer.pow(10, Map.get(@color_map, List.last(colors)))

    if r > 999 do
      {r / 1000, :kiloohms}
    else
      {r, :ohms}
    end
  end

  @spec value(colors :: [atom]) :: integer
  defp value(colors) do
    colors
    |> Enum.take(2)
    |> Enum.map(fn x -> Map.get(@color_map, x) end)
    |> Integer.undigits()
  end
end
