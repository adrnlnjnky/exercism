defmodule Newsletter do
  @doc """
  Recieves a file path with new line separated emails and returns a list of the emails.
  """
  def read_emails(path) do
    {:ok, file} = File.read(path)
    String.split(file, "\n", trim: true)
  end

  @doc """
  Opens a log or fails trying, then returns the PID.
  """
  def open_log(path) do
    File.open!(path, [:write])
  end

  def log_sent_email(pid, email) do
    IO.write(pid, [email <> "\n"])
  end

  def close_log(pid), do: File.close(pid)

  @doc """
  Takes a file path with
   email addresses, 
   path to a log file, and 
   and an anonymous function
  returnes :ok when all emails sent
  """
  def send_newsletter(emails_path, log_path, send_fun) do
    log = open_log(log_path)

    emails_path
    |> read_emails()
    |> Enum.each(fn email ->
      with :ok <- send_fun.(email) do
        log_sent_email(log, email)
      end
    end)

    close_log(log)
  end
end
