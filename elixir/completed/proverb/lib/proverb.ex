defmodule Proverb do
  @doc """
  Generate a proverb from a list of strings.
  """
  @spec recite(strings :: [String.t()]) :: String.t()
  def recite([]), do: ""

  def recite([finish | _] = strings) do
    recite(strings, "", "And all for the want of a #{finish}.\n")
  end

  defp recite([], acc, finish), do: acc <> finish
  defp recite(list, _, finish) when length(list) == 1, do: finish

  defp recite([a | list], acc, finish) when length(list) > 1 do
    [b | _] = list
    recite(list, acc <> verse(a, b), finish)
  end

  defp recite([a, last], acc, finish) do
    recite([], acc <> verse(a, last), finish)
  end

  defp verse(a, b) do
    "For want of a #{a} the #{b} was lost.\n"
  end
end
