defmodule Bob do
  @spec hey(String.t()) :: String.t()
  def hey(input) do
    question = question?(input)
    yelling = yelling?(input)

    cond do
      yelling && question -> "Calm down, I know what I'm doing!"
      question -> "Sure."
      yelling -> "Whoa, chill out!"
      silence?(input) -> "Fine. Be that way!"
      true -> "Whatever."
    end
  end

  defp question?(input) do
    String.trim(input) |> String.ends_with?("?")
  end

  defp yelling?(input) do
    String.upcase(input) == input && String.downcase(input) != input
  end

  defp silence?(input) do
    String.trim(input) == ""
  end
end
