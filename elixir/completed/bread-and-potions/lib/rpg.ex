defmodule RPG do
  defmodule Character do
    defstruct health: 100, mana: 0
  end

  defmodule LoafOfBread do
    defstruct []
  end

  defmodule ManaPotion do
    defstruct strength: 10
  end

  defmodule Poison do
    defstruct []
  end

  defmodule EmptyBottle do
    defstruct []
  end

  defprotocol Edible do
    def eat(item, charactor)
  end

  defimpl Edible, for: LoafOfBread do
    def eat(_, charactor) do
      {nil, Map.update!(charactor, :health, fn x -> x + 5 end)}
    end
  end

  defimpl Edible, for: ManaPotion do
    def eat(potion, charactor) do
      pot_strength = Map.get(potion, :strength)
      updated_char = Map.update!(charactor, :mana, fn x -> x + pot_strength end)
      
      empty_bottle = %EmptyBottle{}

      {empty_bottle, updated_char}
    end
  end

  defimpl Edible, for: Poison do
    def eat(_poison, charactor) do
      empty_bottle = %EmptyBottle{}
      updated_char = Map.update!(charactor, :health, fn x -> x - x end)

      {empty_bottle, updated_char}
    end
  end

end
