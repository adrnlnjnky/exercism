# Use the Plot struct as it is provided
defmodule Plot do
  @enforce_keys [:plot_id, :registered_to]
  defstruct [:plot_id, :registered_to]
end

defmodule CommunityGarden do

  @doc """
  Takes optional keyword list and returns {:ok, pid}

      {:ok, pid} = CommunityGarden.start()
      # => {:ok, #PID<0.112.0>}
  """
  def start() do
    Agent.start(fn -> %{next_id: 1, plots: %{}} end)
  end

  @doc """
  recieves the community pid and returns list of stored plots that have been registered.

      CommunityGarden.list_registrations(pid)
      # => []
  """
  def list_registrations(pid) do
    Agent.get(pid, fn state -> Map.values(state.plots) end)
  end

  def register(pid, register_to) do
    Agent.get_and_update(pid, fn %{next_id: next_id, plots: plots} ->
      plot = %Plot{plot_id: next_id, registered_to: register_to}
      plots = Map.put(plots, plot.plot_id, plot)
      state = %{next_id: next_id + 1, plots: plots}
      {plot, state}
      end)
  end

  def release(pid, plot_id) do
    Agent.update(pid, fn state ->
      plots = Map.delete(state.plots, plot_id)
      %{state | plots: plots}
    end)
  end

  def get_registration(pid, plot_id) do
    plots = Agent.get(pid, fn %{plots: plots} -> plots end)
    Map.get(plots, plot_id, {:not_found, "plot is unregistered"})
  end



end
