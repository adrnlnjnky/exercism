defmodule Garden do
  @students ~w[alice bob charlie david eve fred ginny harriet ileana joseph kincaid larry]a

  @seedlings %{
    "V" => :violets,
    "R" => :radishes,
    "G" => :grass,
    "C" => :clover
  }

  @doc """
    Accepts a string representing the arrangement of cups on a windowsill and a
    list with names of students in the class. The student names list does not
    have to be in alphabetical order.

    It decodes that string into the various gardens for each student and returns
    that information in a map.
  """

  @spec info(String.t(), list) :: map
  def info(info_string, student_names \\ @students) do
    # win_sil =
    info_string
    |> String.split("\n", trim: true)
    |> Stream.map(fn x ->
      String.split(x, "", trim: true)
      |> Stream.map(fn y -> Map.get(@seedlings, y) end)
      |> Stream.chunk_every(2)
    end)
    |> Stream.zip()
    |> Stream.map(fn {x, y} -> List.to_tuple(x ++ y) end)
    |> Stream.concat(Stream.cycle([{}])) |> Stream.take(length(student_names))
    |> Stream.zip_with(student_names, fn x, y -> {y, x} end)
    |> Map.new()

    # student_names
    # |> Enum.sort()
    # |> Enum.zip(win_sil)
    # |> IO.inspect()
    # |> Map.new()
  end
end
