defmodule MatchingBrackets do
  @doc """
  Checks that all the brackets and braces in the string are matched correctly, and nested correctly
  """
  @spec check_brackets(String.t()) :: boolean
  # def check_brackets(str), do: b_match?(str)
  def check_brackets(str) do
    str
    |> String.graphemes()
    |> b_match?()
  end

  defp b_match?(str, acc \\ [])

  defp b_match?([], acc), do: if(acc == [], do: true, else: false)

  defp b_match?([h | str], acc) when h in ["(", "{", "["] do
    b_match?(str, [h | acc])
  end

  defp b_match?([close | _str], []) when close in [")", "}", "]"], do: false

  defp b_match?([close | str], [open | acc]) when close in [")", "}", "]"] do
    case {open, close} do
      {"{", "}"} -> b_match?(str, acc)
      {"(", ")"} -> b_match?(str, acc)
      {"[", "]"} -> b_match?(str, acc)
      _ -> false
    end
  end

  defp b_match?([_h | str], acc) do
    b_match?(str, acc)
  end
end
