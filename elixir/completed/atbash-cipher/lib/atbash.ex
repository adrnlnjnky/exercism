defmodule Atbash do
  @doc """
  Encode a given plaintext to the corresponding ciphertext

  ## Examples

  iex> Atbash.encode("completely insecure")
  "xlnko vgvob rmhvx fiv"
  """
  @spec encode(String.t()) :: String.t()
  def encode(plaintext) do
    plaintext
    |> String.downcase()
    |> String.to_charlist()
    |> Enum.map(&secret/1)
    |> Enum.reject(& (&1 == nil))
    |> Enum.chunk_every(5)
    |> Enum.intersperse([?\s])
    |> List.to_string()
  end

  @spec decode(String.t()) :: String.t()
  def decode(cipher) do
    cipher
    |> String.replace(" ", "")
    |> String.to_charlist()
    |> Enum.map(fn x -> secret(x) end)
    |> List.to_string()
  end

  defp secret(d) when d in ?a..?z, do: ?a + ?z - d
  defp secret(d) when d in ?0..?9, do: d
  defp secret(_d), do: nil

end
