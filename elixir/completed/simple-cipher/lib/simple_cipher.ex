defmodule SimpleCipher do
  @alphabet ~w[ a b c d e f g h i j k l m n o p q r s t u v w x y z ]

  @doc """
  Given a `plaintext` and `key`, encode each character of the `plaintext` by
  shifting it by the corresponding letter in the alphabet shifted by the number
  of letters represented by the `key` character, repeating the `key` if it is
  shorter than the `plaintext`.

  Each letter in the `plaintext` will be encoded with the alphabet of the `key`
  character in the same position. If the `key` is shorter than the `plaintext`,
  repeat the `key`.

  Example:

  plaintext = "testing"
  key = "abc"

  """
  def encode(plaintext, key) do
    key = String.to_charlist(fix_key(String.length(plaintext), <<key::binary>>))

    <<plaintext::binary>>
      |> String.to_charlist()
      |> Enum.zip(key)
      |> Enum.map(fn {x, y} -> slider(x, y) end)
      |> List.to_string()
  end

  defp slider(og, shift) when og in ?a..?z do
    if og + shift - ?a - 1 < ?z do
      og + shift - ?a
    else
      og + shift - ?a - 26
    end
  end

  defp slider(og, shift) when og in ?A..?Z do
    if og + shift - ?A + 1 < ?Z do
      og + shift - ?A
    else
      og + shift - ?A - 26
    end
  end

  defp slider(og, _shift) do
    og
  end

  defp fix_key(l, key) do
    kl = String.length(key)

    cond do
      kl == l -> key
      kl > l -> String.slice(key, 0..l)
      kl < l -> fix_key(l, key <> key)
    end
  end

  # @doc """
  # Given a `ciphertext` and `key`, decode each character of the `ciphertext` by
  # finding the corresponding letter in the alphabet shifted by the number of
  # letters represented by the `key` character, repeating the `key` if it is
  # shorter than the `ciphertext`.

  # The same rules for key length and shifted alphabets apply as in `encode/2`,
  # but you will go the opposite way, so "d" becomes "a", "w" becomes "t",
  # etc..., depending on how much you shift the alphabet.
  # """
  def decode(ciphertext, key) do
    key = String.to_charlist(fix_key(String.length(ciphertext), <<key::binary>>))

    <<ciphertext::binary>>
      |> String.to_charlist()
      |> Enum.zip(key)
      |> Enum.map(fn {x, y} -> uslider(x, y) end)
      |> List.to_string()
  end

  defp uslider(og, shift) when og in ?a..?z do
    if og - (shift - ?a) + 1 > ?a do
      og - (shift - ?a)
    else
      og - (shift - ?a) + 26
    end
  end

  defp uslider(og, shift) when og in ?A..?Z do
    if og - shift - ?A + 1 < ?A do
      og - shift - ?A
    else
      og - shift - ?A + 26
    end
  end

  defp uslider(og, _shift) do
    og
  end

  # @doc """
  # Generate a random key of a given length. It should contain lowercase letters only.
  # """
  def generate_key(length), do: gk(length, "")

  def gk(0, key), do: key

  def gk(length, key) do
    gk(length - 1, key <> Enum.random(@alphabet))
  end
end
