defmodule BasketballWebsite do

  def extract_from_path(data, path) do
    extract_by_key(data, path |> String.split("."))
  end

  def extract_by_key(data, []), do: data
  def extract_by_key(nil, _), do: nil

  def extract_by_key(data, [head | paths]) do
    extract_by_key(extract_by_key(data, head), paths)
  end

  def extract_by_key(data, path) do
    data[path]
  end


  def get_in_path(data, path) do
    get_in(data, path |> String.split("."))
  end
end
