defmodule NameBadge do
  def print(id, name, department) do

    id_checked = if id != nil do
      "[#{id}]" <> " - "
    else
      ""
    end

    department_checked = if department != nil do
      String.upcase(department)
    else
      "OWNER"
    end

    id_checked <> name <> " - " <> department_checked
  end
end
