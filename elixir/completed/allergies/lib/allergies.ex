defmodule Allergies do
  @alergies ~w[
    eggs
    peanuts
    shellfish
    strawberries
    tomatoes
    chocolate
    pollen
    cats
  ]

  @doc """
  List the allergies for which the corresponding flag bit is true.
  """
  @spec list(non_neg_integer) :: [String.t()]
  def list(flags) do
    list(flags, [])
  end

  defp list(0, acc), do: Enum.reject(acc, fn x -> x == nil end)

  defp list(flags, acc) when rem(flags, 2) == 0 do
    count = get_count(flags, 0)

    if count > Enum.count(@alergies) do
      acc
    else
      alergy = Enum.at(@alergies, count)
      list(flags - get_flag(count, 1), List.insert_at(acc, -1, alergy))
    end
  end

  defp list(flags, _acc) do
    list(flags - 1, [Enum.at(@alergies, 0)])
  end

  def get_count(1, count), do: count

  def get_count(flags, count) do
    get_count(div(flags, 2), count + 1)
  end

  def get_flag(0, flag), do: flag

  def get_flag(count, flag) do
    get_flag(count - 1, flag * 2)
  end

  @doc """
  Returns whether the corresponding flag bit in 'flags' is set for the item.
  """
  @spec allergic_to?(non_neg_integer, String.t()) :: boolean
  def allergic_to?(flags, item) do
    list = list(flags)

    Enum.any?(list, fn x -> x == item end)
  end
end
