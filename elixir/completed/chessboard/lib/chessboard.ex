defmodule Chessboard do

  @doc """
  Returns a range of integers from 1 to 8

    Chessboard.rank_range()

  """
  def rank_range, do: 1..8

  @doc """
  Returns a range of code points of the uppercase A to H.

    Chessboard.file_range()

  """
  def file_range, do: ?A..?H

  @doc """
  Returns a List of integers from 1 to 8

   Chessboard.ranks()
    # => [1, 2, 3, 4, 5, 6, 7, 8]
  """
  def ranks, do: Enum.to_list(rank_range())

  @doc """
  Returns a List of letters from "A" to "H"

    Chessboard.ranks()
    # => ["A", "B", "C", "D", "E", "F", "G", "H"]
    
  """
  def files do
    file_range()
      |> Enum.to_list()
      |> Enum.map(fn x -> <<x>> end)
  end
end
