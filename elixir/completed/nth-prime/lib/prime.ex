defmodule Prime do
  # @negativenumbererror "We don't do negative numbers"
  # @stringerror "Strings can't be prime"

  require Integer

  @doc """
  Generates the nth prime.
  """
  @spec nth(non_neg_integer) :: non_neg_integer
  def nth(0), do: raise "no 0 Prime"
  def nth(count) do
    ranger(count, 0, 1, 2)
  end

  defp ranger(count, found, _place_holder, prime) when count == found, do: prime
  defp ranger(count, found, place_holder, prime) do
      if is_prime?(place_holder) == true do
        ranger(count, found + 1, (place_holder + 1), place_holder)
      else
        ranger(count, found, (place_holder + 1), prime)
      end
  end

  defp is_prime?(_number = 1), do: false
  defp is_prime?(_number = 2), do: true
  defp is_prime?(number) when Integer.is_even(number) == true, do: false
  defp is_prime?(number) do
    for n <- 2..(number - 1) do
      if rem(number, n) == 0, do: false, else: true
    end
    |> Enum.all?(fn x -> x == true end)
  end

end
