defmodule BoutiqueInventory do
  def sort_by_price(inventory) do
    Enum.sort_by(inventory, &(&1.price), :asc)
  end

  def with_missing_price(inventory) do
    Enum.filter(inventory, &(&1.price == nil)) 
  end

  def increase_quantity(item, count) do
    item
    |> Map.update(:quantity_by_size, %{}, fn quantities ->
      quantities
      |> Enum.map(fn {size, quantity} ->  {size, quantity + count} end)
      |> Enum.reverse()
      |> Enum.into(%{})
    end)

  end

  def total_quantity(item) do
    Enum.reduce(item[:quantity_by_size], 0, fn {_, q}, acc -> q + acc end)
  end
end


