defmodule Integer.Guards  do
  defguard bad_number(value) when is_integer(value) == false
end


defmodule LucasNumbers do
  @moduledoc """
  Lucas numbers are an infinite sequence of numbers which build progressively
  which hold a strong correlation to the golden ratio (φ or ϕ)

  E.g.: 2, 1, 3, 4, 7, 11, 18, 29, ...
  """
  import Integer.Guards
  @error "count must be specified as an integer >= 1"

  def generate(count) do
    generating(count)
  end

  defp generating(count) when bad_number(count), do: raise ArgumentError, @error
  defp generating(count) when count < 1, do: raise ArgumentError, @error
  defp generating(count) do
          stream = Stream.unfold({2, 1}, fn {a, b} -> {a, {b, a + b}} end)
          Enum.take(stream, count)
  end

end


#  This is a conditional clause option but I felt like the private function with guard clauses was the more idiomatic
#  option. this plug in instead of calling generating(count)
#
    # cond do
    #   bad_number(count) -> raise ArgumentError, "count must be specified as an integer >= 1"
    #   count < 1 -> raise ArgumentError, "count must be specified as an integer >= 1"
    #   count == 1 -> [2]
    #   count == 2 -> [2, 1]
    #   count > 2 -> 
    #       stream = Stream.unfold({2, 1}, fn {a, b} -> {a, {b, a + b}} end)
    #       Enum.take(stream, count)
    # end
