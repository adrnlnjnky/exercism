defmodule CaptainsLog do
  @planetary_classes ["D", "H", "J", "K", "L", "M", "N", "R", "T", "Y"]
  @date_span 41000..42000

  @doc """
  generates a random planet class from @planetary_classes
  """
  def random_planet_class(), do: Enum.random(@planetary_classes)

  @doc """
  creates a random ship number with proper prepended letters
  """
  def random_ship_registry_number(), do: "NCC-#{Enum.random(1000..9999)}"

  @doc """
    creates a stardate between 41000.0 and 42000.0
  """
  def random_stardate() do
    integer = @date_span |> Enum.random() |> div(1)
    decimal = if integer == 42000, do: 0.0, else: :rand.uniform()

    integer + decimal 
  end

  @doc """
  checks for a float, then rounds it to one point and returns a string.
  """
  def format_stardate(stardate) do
    if is_integer(stardate), do: raise ArgumentError
    "#{Float.round(stardate, 1)}"
  end
end
