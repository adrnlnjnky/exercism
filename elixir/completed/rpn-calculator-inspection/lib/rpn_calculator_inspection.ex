defmodule RPNCalculatorInspection do
  def start_reliability_check(calculator, input) do
    {:ok, pid} = Task.start_link(fn -> calculator.(input) end)
    %{input: input, pid: pid}
  end

  def await_reliability_check_result(%{pid: pid, input: input}, results) do
    receive do
      {:EXIT, ^pid, :normal} -> Map.put(results, input, :ok)
      {:EXIT, ^pid, _} -> Map.put(results, input, :error)
    after
      100 -> Map.put(results, input, :timeout)
    end

  end

  # def reliability_check(calculator, inputs) do
  #   oldvalue = Process.flag(:trap_exit, true)
  #   result_map =
  #     Enum.reduce(inputs, %{}, fn input, acc ->
  #                   start_reliability_check(calculator, input)
  #                   |> await_reliability_check_result(acc) end)
  #   Process.flag(:trap_exit, oldvalue)
  #   result_map
  # end


  # tap is useful for running synchronous side effects in a pipeline
  def reliability_check(calculator, inputs) do
    flag = Process.flag(:trap_exit, true)
    Enum.map(inputs, &start_reliability_check(calculator, &1))
    |> Enum.reduce(%{}, &await_reliability_check_result/2)
    |> tap(fn _ -> Process.flag(:trap_exit, flag) end)
  end

  def correctness_check(calculator, inputs) do
    inputs
    |> Enum.map(&Task.async(fn -> calculator.(&1) end))
    |> Enum.map(&Task.await(&1, 100))
  end
end
