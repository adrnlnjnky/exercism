defmodule TwelveDays.Say do

  @doc """
  Translate a positive integer into English.
  """
  @spec in_english(integer) :: {atom, String.t()}

  def in_english(number) when number < 1 or number > 12,
    do: {:error, "days are out of range"}

  def in_english(number) when is_integer(number), do: english_words(number)

  def in_english(_number), do: {:error, "Must be a number"}


  defp english_words(1), do: "first"
  defp english_words(2), do: "second"
  defp english_words(3), do: "third"
  defp english_words(4), do: "fourth"
  defp english_words(5), do: "fifth"
  defp english_words(6), do: "sixth"
  defp english_words(7), do: "seventh"
  defp english_words(8), do: "eighth"
  defp english_words(9), do: "ninth"
  defp english_words(10), do: "tenth"
  defp english_words(11), do: "eleventh"
  defp english_words(12), do: "twelfth"

end
