defmodule TwelveDays do
  @verses [
    " twelve Drummers Drumming,",
    " eleven Pipers Piping,",
    " ten Lords-a-Leaping,",
    " nine Ladies Dancing,",
    " eight Maids-a-Milking,",
    " seven Swans-a-Swimming,",
    " six Geese-a-Laying,",
    " five Gold Rings,",
    " four Calling Birds,",
    " three French Hens,",
    " two Turtle Doves,",
    " and",
    " a Partridge in a Pear Tree."
  ]

  @doc """
  Given a `number`, return the song's verse for that specific day, including
  all gifts for previous days in the same line.
  """
  @spec verse(number :: integer) :: String.t()
  def verse(number) do
    day = english_words(number)
    "On the #{day} day of Christmas my true love gave to me:" <> gift(number)
  end

  defp gift(1), do: gifts(1)
  defp gift(n), do: gifts(n + 1)

  defp gifts(n) do
    @verses
    |> Enum.reverse()
    |> Enum.take(n)
    |> Enum.reverse()
    |> List.to_string()
  end

  @doc """
  Given a `starting_verse` and an `ending_verse`, return the verses for each
  included day, one per line.
  """
  @spec verses(starting_verse :: integer, ending_verse :: integer) :: String.t()
  def verses(starting_verse, ending_verse) do
    starting_verse..ending_verse
    |> Enum.to_list()
    |> Enum.map_join("\n", &verse/1)
  end

  @doc """
  Sing all 12 verses, in order, one verse per line.
  """
  @spec sing() :: String.t()
  def sing do
    verses(1, 12)
  end

  defp english_words(1), do: "first"
  defp english_words(2), do: "second"
  defp english_words(3), do: "third"
  defp english_words(4), do: "fourth"
  defp english_words(5), do: "fifth"
  defp english_words(6), do: "sixth"
  defp english_words(7), do: "seventh"
  defp english_words(8), do: "eighth"
  defp english_words(9), do: "ninth"
  defp english_words(10), do: "tenth"
  defp english_words(11), do: "eleventh"
  defp english_words(12), do: "twelfth"
end
