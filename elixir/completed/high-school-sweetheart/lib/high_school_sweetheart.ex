defmodule HighSchoolSweetheart do









  def first_letter(name) do
    String.trim(name) |> String.at(0)
  end

  def initial(name) do
    i = first_letter(name) |> String.capitalize()
    i <> "."

  end

  def initials(full_name) do
    [first, last ] = String.split(full_name, " ", trim: true)
    f = initial(first)
    l = initial(last)
    f <> " " <> l
  end

  def pair(full_name1, full_name2) do
    i1 = initials(full_name1)
    i2 = initials(full_name2)

"""
     ******       ******
   **      **   **      **
 **         ** **         **
**            *            **
**                         **
**     #{i1}  +  #{i2}     **
 **                       **
   **                   **
     **               **
       **           **
         **       **
           **   **
             ***
              *
"""
  end
end
