defmodule Rules do
  def eat_ghost?(power_pellet_active = p, touching_ghost =t), do: p and t

  def score?(touching_power_pellet, touching_dot) do
    if touching_power_pellet == false and touching_dot == false do
      false
    else
      true
    end
  end

  def lose?(power_pellet_active, touching_ghost) do
    if power_pellet_active == false and touching_ghost == true do
      true
    end
  end

  def win?(has_eaten_all_dots, power_pellet_active, touching_ghost) do
    if has_eaten_all_dots == true and lose?(power_pellet_active, touching_ghost) != true do
      true
    else
      false
    end
  end
end
