defmodule PrimeFactors do
  @doc """
  Compute the prime factors for 'number'.

  The prime factors are prime numbers that when multiplied give the desired
  number.

  The prime factors of 'number' will be ordered lowest to highest.
  """
  @spec factors_for(pos_integer) :: [pos_integer]
  def factors_for(number) when number <= 0, do: "that's a no no"

  def factors_for(number) do
    f(number)
  end

  defp f(n, cf \\ 2, acc \\ [])
  defp f(1, _, acc), do: acc

  defp f(n, cf, acc) when rem(n, cf) == 0, do: f(div(n, cf), cf, List.insert_at(acc, -1, cf))
  defp f(n, cf, acc), do: f(n, cf + 1, acc)
end
