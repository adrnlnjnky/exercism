defmodule Scrabble do
  @one ~w[A E I O U L N R S T]
  @two ~w[D G]
  @three ~w[B C M P]
  @four ~w[F H V W Y]
  @five ~w[K]
  @eight ~w[J X]
  @ten ~w[Q Z]

  @doc """
  Calculate the scrabble score for the word.
  """
  @spec score(String.t()) :: non_neg_integer
  def score(word) do
    word
    |> String.upcase()
    |> String.split("", trim: true)
    |> Enum.reduce(0, fn x, acc -> adder(x) + acc end)
  end

  defp adder(l) when l in @one, do: 1 
  defp adder(l) when l in @two, do: 2
  defp adder(l) when l in @three, do: 3
  defp adder(l) when l in @four, do: 4
  defp adder(l) when l in @five, do: 5
  defp adder(l) when l in @eight, do: 8
  defp adder(l) when l in @ten, do: 10
  defp adder(_), do: 0 

end
