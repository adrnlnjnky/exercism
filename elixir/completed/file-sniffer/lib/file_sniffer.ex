defmodule FileSniffer do

  @error "Warning, file format and file extension do not match."

  @doc """
    Takes in a file extention and returns the media type
  """
  def type_from_extension(extension) do
    case extension do
      "exe" -> "application/octet-stream"
      "bmp" -> "image/bmp"
      "png" -> "image/png"
      "jpg" -> "image/jpg"
      "gif" -> "image/gif"
    end

  end

  @doc """
  Takes in a file (binary) and returns a string of the media type.
  """
  def type_from_binary(file_binary) do 
    get_type_from_binary(file_binary)
  end

  defp get_type_from_binary(<<0x7F, 0x45, 0x4C, 0x46, _::binary>>), do: "application/octet-stream" 
  defp get_type_from_binary(<<0x42, 0x4D, _::binary>>), do: "image/bmp"
  defp get_type_from_binary(<<0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A, _::binary>>), do: "image/png"
  defp get_type_from_binary(<<0xFF, 0xD8, 0xFF, _::binary>>), do: "image/jpg"
  defp get_type_from_binary(<<0x47, 0x49, 0x46, _::binary>>), do: "image/gif"


  @doc """
  Takes in a binary file and an extension and verifies they match.
  """
  def verify(file_binary, extension) do
    file_type = type_from_binary(file_binary)
    ext_type = type_from_extension(extension)

    cond do
      file_type != ext_type -> {:error, @error}
      true -> {:ok, file_type}
    end
  end

  # defp convert_binary_to_string(binary) do
  #     case binary do
  #       "\d" -> "exe"
  #       "G" -> "gif"
  #       <<137>> -> "png"
  #       <<255>> -> "jpg"
  #       "B" -> "bmp"
  #     end
  # end


end
