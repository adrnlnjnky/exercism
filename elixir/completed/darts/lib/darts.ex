defmodule Darts do
  @type position :: {number, number}

  @doc """
  Calculate the score of a single dart hitting a target
  """
  @spec score(position) :: integer
  def score({x, y}) do
    do_math({x,y}) |> sort()
  end

  defp sort(dist) do
    cond do
      dist > 10 -> 0
      dist > 5 -> 1
      dist > 1 -> 5
      true -> 10
    end
  end

  defp do_math({x,y}) do
    :math.pow(x, 2) + :math.pow(y, 2)
    |> :math.sqrt()

  end
end
