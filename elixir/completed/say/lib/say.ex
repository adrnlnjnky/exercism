defmodule Say do
  @doc """
  Translate a positive integer into English.
  """
  @spec in_english(integer) :: {atom, String.t()}
  def in_english(0), do: rv("zero")

  def in_english(number) when number < 0 or number > 999_999_999_999,
    do: {:error, "number is out of range"}

  def in_english(number) when is_integer(number), do: in_english(number, "")
  def in_english(_number), do: {:error, "Must be a number"}

  defp in_english(0, acc), do: rv(String.trim(acc))

  defp in_english(number, acc) when number > 999_999_999 do
    b = div(number, 1_000_000_000)
    in_english(number - b * 1_000_000_000, acc <> rv(in_english(b)) <> " billion ")
  end

  defp in_english(number, acc) when number > 999_999 do
    m = div(number, 1_000_000)
    in_english(number - m * 1_000_000, acc <> rv(in_english(m)) <> " million ")
  end

  defp in_english(number, acc) when number > 999 do
    t = div(number, 1000)
    in_english(number - t * 1000, acc <> rv(in_english(t)) <> " thousand ")
  end

  defp in_english(number, acc) when number > 99 do
    t = div(number, 100)
    in_english(number - t * 100, acc <> english_words(t) <> " hundred ")
  end

  defp in_english(number, acc) when number > 20 do
    t = div(number, 10) * 10
    in_english(number - t, acc <> english_words(t) <> "-")
  end

  defp in_english(number, acc) when number <= 20 do
    in_english(0, acc <> english_words(number))
  end

  defp rv({:ok, value}), do: value
  defp rv(value), do: {:ok, value}

  defp english_words(1), do: "one"
  defp english_words(2), do: "two"
  defp english_words(3), do: "three"
  defp english_words(4), do: "four"
  defp english_words(5), do: "five"
  defp english_words(6), do: "six"
  defp english_words(7), do: "seven"
  defp english_words(8), do: "eight"
  defp english_words(9), do: "nine"
  defp english_words(10), do: "ten"
  defp english_words(11), do: "eleven"
  defp english_words(12), do: "twelve"
  defp english_words(13), do: "thirteen"
  defp english_words(14), do: "fourteen"
  defp english_words(15), do: "fifteen"
  defp english_words(16), do: "sixteen"
  defp english_words(17), do: "seventeen"
  defp english_words(18), do: "eighteen"
  defp english_words(19), do: "nineteen"

  defp english_words(20), do: "twenty"
  defp english_words(30), do: "thirty"
  defp english_words(40), do: "forty"
  defp english_words(50), do: "fifty"
  defp english_words(60), do: "sixty"
  defp english_words(70), do: "seventy"
  defp english_words(80), do: "eighty"
  defp english_words(90), do: "ninety"

end
