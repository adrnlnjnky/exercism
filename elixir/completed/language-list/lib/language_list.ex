defmodule LanguageList do
  def new() do
    []
  end

  def add(list, language) do
    List.insert_at(list, 0, language)
  end

  def remove(list) do
    {_first, last} = List.pop_at(list, 0)
    last
  end

  def first(list) do
    List.first(list)
  end

  def count(list) do
    length(list)
  end

  def exciting_list?([]), do: false
  def exciting_list?(list) do
    {language, list} = List.pop_at(list, 0)
    case language do
      "Elixir" -> true
      _ -> exciting_list?(list)
    end
  end
end
