defmodule Anagram do
  @doc """
  Returns all candidates that are anagrams of, but not equal to, 'base'.
  """
  @spec match(String.t(), [String.t()]) :: [String.t()]
  def match(base, candidates),
    do: Enum.filter(candidates, &anagram(base, &1))

  defp anagram(base, word) do
    base = String.downcase(base)
    word = String.downcase(word)

    word != base && sort(word) == sort(base)
  end

  defp sort(word), do: word |> String.split("", trim: true) |> Enum.sort()
end
