defmodule Grains do
  @doc """
  Calculate two to the power of the input minus one.
  """
  @spec square(pos_integer()) :: {:ok, pos_integer()} | {:error, String.t()}
  def square(number) do
    if 0 < number && number < 65,
      do: {:ok, Integer.pow(2, number - 1)},
      else: {:error, "The requested square must be between 1 and 64 (inclusive)"}
  end

  @doc """
  Adds square of each number from 1 to 64.
  """
  @spec total :: {:ok, pos_integer()}
  def total do
    {:ok,
     for n <- 1..64 do
      square(n) |> then(&elem(&1,1))
     end
     > Enum.sum()
    }
  end
end
