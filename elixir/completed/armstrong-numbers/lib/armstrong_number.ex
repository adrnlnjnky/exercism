defmodule ArmstrongNumber do
  @moduledoc """
  Provides a way to validate whether or not a number is an Armstrong number
  """

  @spec valid?(integer) :: boolean
  def valid?(number) when is_integer(number) != true, do: raise "Only integers can be armstrong numbers."
  def valid?(number) when number < 0, do: false
  def valid?(0), do: true

  def valid?(number) do
    d = Integer.digits(number)
    armstrongy = Enum.reduce(d, 0, fn x, acc -> Integer.pow(x, length(d)) + acc end)

    armstrongy == number
  end
end

