defmodule House do
  @verses [
    "the house that Jack built.\n",
    "the malt that lay in ",
    "the rat that ate ",
    "the cat that killed ",
    "the dog that worried ",
    "the cow with the crumpled horn that tossed ",
    "the maiden all forlorn that milked ",
    "the man all tattered and torn that kissed ",
    "the priest all shaven and shorn that married ",
    "the rooster that crowed in the morn that woke ",
    "the farmer sowing his corn that kept ",
    "the horse and the hound and the horn that belonged to "
  ]

  @doc """
  Return verses of the nursery rhyme 'This is the House that Jack Built'.
  """
  @spec recite(start :: integer, stop :: integer) :: String.t()
  def recite(start, stop) do
    for verse <- start..stop, into: "" do
      build_verse(verse)
    end
  end

  defp build_verse(verse) do
    verse =
      @verses
      |> Enum.slice(0, verse)
      |> Enum.reverse()
      |> List.to_string()

    "This is " <> verse
  end
end
