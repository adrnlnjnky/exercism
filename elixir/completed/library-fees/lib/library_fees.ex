defmodule LibraryFees do
  def datetime_from_string(string), do: NaiveDateTime.from_iso8601!(string)

  def before_noon?(datetime) do
      datetime
      |> NaiveDateTime.to_time()
      |> Time.compare(~T[12:00:00])
      |> case do
        :lt -> true
        _ -> false
      end
  end

  def return_date(checkout_datetime) do
    morning = before_noon?(checkout_datetime)
    date = NaiveDateTime.to_date(checkout_datetime)
    case morning do
      true -> Date.add(date, 28)
      false -> Date.add(date, 29)
    end
  end

  def days_late(planned_return_date, actual_return_datetime) do
    ret_date = NaiveDateTime.to_date(actual_return_datetime)
    dif = Date.diff(ret_date, planned_return_date)
    if dif < 0, do: 0, else: dif
  end

  def monday?(datetime) do
    datetime
    |> NaiveDateTime.to_date()
    |> Date.day_of_week()
    |> case do
      1 -> true
      _ -> false
    end
  end

  def calculate_late_fee(checkout, return, rate) do
    checkout = datetime_from_string(checkout)
    returned = datetime_from_string(return)
    due_date = return_date(checkout)
    late = days_late(due_date, returned)
    is_monday = monday?(returned)
    case is_monday do
      true -> floor((late * rate) * 0.5)
      false -> floor(late * rate)
    end
  end

end
