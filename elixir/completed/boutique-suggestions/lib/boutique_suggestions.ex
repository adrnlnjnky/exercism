defmodule BoutiqueSuggestions do

  @doc """
  take a list of tops
       a list of bottoms
    and a keyword list of options

  *. creates a cartesian product of the tops and bottoms
  *. filters by same color top/bottom combo
  *. filters by :maximum_price

    tops = [
      %{item_name: "Dress shirt", base_color: "blue", price: 35},
      %{item_name: "Casual shirt", base_color: "black", price: 20}
    ]
    bottoms = [
      %{item_name: "Jeans", base_color: "blue", price: 30},
      %{item_name: "Dress trousers", base_color: "black", price: 75}
    ]
    BoutiqueSuggestions.get_combinations(tops, bottoms, maximum_price: 50)
    # => [
    #      {%{item_name: "Casual shirt", base_color: "black", price: 20},
    #       %{item_name: "Jeans", base_color: "blue", price: 30}}
    #    ]
  
  """
  def get_combinations(tops, bottoms, options \\ [maximum_price: 100.00]) do
    options_with_default = Keyword.get(options, :maximum_price, 100)
    helper(tops, bottoms, options_with_default)
  end

  defp helper(tops, bottoms, options) do
    combos =
    for top <- tops do
      for bottom <- bottoms, 
        top.base_color != bottom.base_color && 
          options >= top.price + bottom.price do
        {top, bottom}
      end
    end
    List.flatten(combos) end
end
