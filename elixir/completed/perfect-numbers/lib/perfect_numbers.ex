defmodule PerfectNumbers do
  @doc """
  Determine the aliquot sum of the given `number`, by summing all the factors
  of `number`, aside from `number` itself.

  Based on this sum, classify the number as:

  :perfect if the aliquot sum is equal to `number`
  :abundant if the aliquot sum is greater than `number`
  :deficient if the aliquot sum is less than `number`
  """
  @spec classify(number :: integer) :: {:ok, atom} | {:error, String.t()}
  def classify(number) when number <= 0,
    do: {:error, "Classification is only possible for natural numbers."}

  def classify(1), do: {:ok, :deficient}

  def classify(number) do
    psums = sum_divisors(number)

    cond do
      psums > number -> {:ok, :abundant}
      psums == number -> {:ok, :perfect}
      psums < number -> {:ok, :deficient}
    end
  end

  def sum_divisors(n) do
    for j <- 1..(n - 1), rem(n, j) == 0, reduce: 0 do
      acc -> acc + j
    end
  end
end
