defmodule Sublist do
  @doc """
  Returns whether the first list is a sublist or a superlist of the second list
  and if not whether it is equal or unequal to the second list.
  """
  def compare(a, b) do
    cond do
      a === b ->
        :equal

      a == [] ->
        :sublist

      b == [] ->
        :superlist

      length(a) > length(b) ->
        if Enum.any?(slices(a, length(b)), fn x -> x === b end) == true,
          do: :superlist,
          else: :unequal

      length(a) < length(b) ->
        if Enum.any?(slices(b, length(a)), fn x -> x === a end) == true,
          do: :sublist,
          else: :unequal

      true ->
        :unequal
    end
  end

  def slices(s, size), do: Enum.chunk_every(s, size, 1, :discard)
end
