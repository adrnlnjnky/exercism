defmodule Dominoes do
  @type domino :: {1..6, 1..6}

  @doc """
  chain?/1 takes a list of domino stones and returns boolean indicating if it's
  possible to make a full chain
  """
  @spec chain?(dominoes :: [domino]) :: boolean
  def chain?([]), do: true
  def chain?([{a, b}] = dominoes) when length(dominoes) == 1, do: a == b

  def chain?([{head, tail} | dominoes]) do
    last = build_chain(dominoes, tail, false)

    last2 =
      if head == last,
        do: true,
        else: build_chain(dominoes, head, false)

    tail == last2
  end

  defp build_chain([], _to_match, _flipped), do: true

  defp build_chain(dominoes, to_match, flipped) do
    {{head, tail}, dominoes} = List.keytake(dominoes, to_match, 0)

    cond do
      head == to_match ->
        build_chain(dominoes, tail, false)

      tail == to_match ->
        build_chain(dominoes, head, false)

      true ->
        if flipped == false,
          do: Enum.map(dominoes, &Enum.reverse/1) |> build_chain(to_match, true),
          else: false
    end
  end
end
