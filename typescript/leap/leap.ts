export function isLeap(year: number): boolean {

    let divBy4 = year % 4
    let div_by_100 = year % 100
    let div_by_400 = year % 400

    if (divBy4 != 0)
    {
        return false
    }
    else if (div_by_400 == 0)
    {
        return true
    } 
    else if (div_by_100 == 0)
    {
        return false
    }
    else
    {
        return true
    }

    //     except when divisible by 400
}
