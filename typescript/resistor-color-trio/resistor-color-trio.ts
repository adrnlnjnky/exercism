export function decodedResistorValue(bands: string[]) {

    // const colorList = [
    //     {color: 'black', value: 0},
    //     {color: 'brown', value: 1},
    //     {color: 'red', value: 2},
    //     {color: 'orange', value: 3},
    //     {color: 'yellow', value: 4},
    //     {color: 'green',  value: 5},
    //     {color: 'blue', value: 6},
    //     {color: 'violet', value: 7},
    //     {color: 'grey', value: 8},
    //     {color: 'white', value: 9}
    //      ];

    const colorList = ['black', 'brown', 'red', 'orange', 'yellow', 'green', 'blue', 'violet', 'grey', 'white', ];
    const first = colorList.indexOf(bands[0], 0);
    const second = colorList.indexOf(bands[1], 0);
    const exponent = colorList.indexOf(bands[2], 0);
    const merge = first * 10 + second

    if (exponent == 0)
    {
        const ohms = merge
        return ohms + " ohms"
    }
    else if (exponent < 2)
    {
        const ohms = merge ;
        return ohms * Math.pow(10, exponent) + " ohms"
    }
    else
    {
        const ohms = merge ;
        return (ohms * Math.pow(10, exponent))/1000 + " kiloohms"
    }
}


