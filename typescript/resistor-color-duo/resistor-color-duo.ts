
export function decodedValue(bands: string[]) {

    const colorList = ['black', 'brown', 'red', 'orange', 'yellow', 'green', 'blue', 'violet', 'grey', 'white', ];
    const first = colorList.indexOf(bands[0], 0);
    const second = colorList.indexOf(bands[1], 0);
    return (first * 10 + second)

}


