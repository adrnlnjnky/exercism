EXPECTED_BAKE_TIME = 40



# TODO: consider defining the 'PREPARATION_TIME' constant

#       equal to the time it takes to prepare a single layer

def bake_time_remaining(been_in):
    """Calculate the bake time remaining.

    :param elapsed_bake_time: int baking time already elapsed.
    :return: int remaining bake time derived from 'EXPECTED_BAKE_TIME'.

    Function that takes the actual minutes the lasagna has been in the oven as
    an argument and returns how many minutes the lasagna still needs to bake
    based on the `EXPECTED_BAKE_TIME`.
    """
    return EXPECTED_BAKE_TIME - been_in


def preparation_time_in_minutes(layers):
    """Calclatestheprparationtimeiventhenmeroflayers
    :param been_in howlonit'seenin
    :return time remainin
    """
    time= layers * 2

    return time


def  elapsed_time_in_minutes(layers, elapsed_time):
    """elapse_time_in_minutes
    :param been_in howlonit'seenin
    :return time remainin
    """
    return preparation_time_in_minutes(layers) + elapsed_time
