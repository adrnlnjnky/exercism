// @ts-check
//
// The line above enables type checking for this file. Various IDEs interpret
// the @ts-check directive. It will give you helpful autocompletion when
// implementing this exercise.

/**
 * Calculates the total bird count.
 *
 * @param {number[]} birdsPerDay
 * @returns {number} total bird count
 */
export function totalBirdCount(birdsPerDay) {
    var total_birds = 0
    for (let i = 0; i < birdsPerDay.length; i++) {
        total_birds += birdsPerDay[i]
    }
    return total_birds
}

/**
 * Calculates the total number of birds seen in a specific week.
 *
 * @param {number[]} birdsPerDay
 * @param {number} week
 * @returns {number} birds counted in the given week
 */
export function birdsInWeek(birdsPerDay, week) {
    var total_birds = 0
    let i = (week * 7) - 7
    let e = i + 7
    for ( i; i < e; i++) {
        total_birds += birdsPerDay[i]
    }
    return total_birds
}

/**
 * Fixes the counting mistake by increasing the bird count
 * by one for every second day.
 *
 * @param {number[]} birdsPerDay
 * @returns {number[]} corrected bird count data
 */
export function fixBirdCountLog(birdsPerDay) {
    for (let i = 0; i < birdsPerDay.length; i) {
        birdsPerDay[i]++
        i = i + 2
    }
    return birdsPerDay
}
